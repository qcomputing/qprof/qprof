# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

# Primitive Gates Times
PGT = {
    "U1": 0,
    "U2": 100 + 20,
    "U3": 100 + 20 + 100 + 20,
    "CX": 100 + 20 + 347 + 20 + 100 + 20 + 347 + 20,
}

gate_times = {
    "X": PGT["U3"],
    "Y": PGT["U3"],
    "Z": PGT["U1"],
    "H": PGT["U2"],
    "S": PGT["U1"],
    "T": PGT["U1"],
    "RX": PGT["U3"],
    "RY": PGT["U3"],
    "RZ": PGT["U1"],
    "PH": PGT["U1"],
    "CNOT": PGT["CX"],
    "C-PH": PGT["U1"] + PGT["CX"] + PGT["U1"] + PGT["CX"] + PGT["U1"],
    "CCNOT": (
        PGT["U2"]
        + PGT["CX"]
        + PGT["U1"]
        + PGT["CX"]
        + PGT["U1"]
        + PGT["CX"]
        + PGT["U1"]
        + PGT["CX"]
        + PGT["U1"]
        + PGT["U1"]
        + PGT["U2"]
        + PGT["CX"]
        + PGT["U1"]
        + PGT["U1"]
        + PGT["CX"]
    ),
}
gate_times.update(PGT)
