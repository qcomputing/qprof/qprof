# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================
import typing as ty
import json

from qprof.data import ProgramData
from qprof.exporters import BaseExporter


class RawExporter(BaseExporter):
    def __init__(self, indent: ty.Optional[int] = 2):
        """Initialise the RawExporter.

        :param indent: Indent size used to export the raw dictionary as JSON. A value
            of None outputs a compact JSON.
        """
        super().__init__()
        self.indent = indent

    def export(self, data: ProgramData) -> str:
        """Returns a JSON representation of the program data."""
        return json.dumps(data.to_dict(), indent=self.indent)
