# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

import abc
import typing as ty

from qprof.data import ProgramData


class BaseExporter(abc.ABC):
    def __init__(self):
        """Abstract interface for the exporter classes

        Any exporter should inherit from this abstract base class. The most important
        (and only for now) method is "export".
        """

    @abc.abstractmethod
    def export(self, data: ProgramData) -> ty.Union[str, bytes]:
        """Export the given data.

        :param data: data of the program to export
        :return: a representation of the exported data
        """
