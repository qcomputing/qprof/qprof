# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

import pytest as pt

from qprof.routine_tree import RoutineNode, RoutineNodeFactory
from ..utils import FakeRoutineWrapper


def test_get_gate_time():
    assert RoutineNode._get_gate_time("a", {"A": 3}) == 3
    assert RoutineNode._get_gate_time("A", {"A": 3}) == 3
    assert RoutineNode._get_gate_time("a", {"A": 3, "B": 3}) == 3
    assert RoutineNode._get_gate_time("A", {"A": 3, "B": 3}) == 3
    with pt.raises(RuntimeError):
        RoutineNode._get_gate_time("a", {"b": 2})


def test_empty_routine():
    empty = FakeRoutineWrapper("empty")
    factory = RoutineNodeFactory()
    node = RoutineNode(empty, factory, gate_times={"EMPTY": 0})
    assert node.name == "empty"
    assert node.is_base
    assert node.total_time == 0
    assert node.self_time == 0
    assert node.subroutines_times == 0


def test_x_routine():
    x = FakeRoutineWrapper("x")
    factory = RoutineNodeFactory()
    node = RoutineNode(x, factory, gate_times={"X": 74})
    assert node.name == "x"
    assert node.is_base
    assert node.total_time == 74
    assert node.self_time == 74
    assert node.subroutines_times == 0


def test_calls_x_routine():
    x = FakeRoutineWrapper("x")
    calls_x = FakeRoutineWrapper("calls_x", [x])
    factory = RoutineNodeFactory()
    node = RoutineNode(calls_x, factory, gate_times={"X": 74})
    assert node.name == "calls_x"
    assert not node.is_base
    assert node.total_time == 74
    assert node.self_time == 0
    assert node.subroutines_times == 74
