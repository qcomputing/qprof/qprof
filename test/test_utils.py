# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from test.utils import _is_iterable, dict_eq, FakeRoutineWrapper


def test_is_iterable():
    assert _is_iterable(list())
    assert _is_iterable(tuple())
    assert _is_iterable(dict())
    assert _is_iterable(set())
    assert _is_iterable(iter([i for i in range(3)]))
    assert _is_iterable(range(4))
    assert not _is_iterable(1)
    assert _is_iterable("Hello")
    assert not _is_iterable(1.0)


def test_dict_eq():
    assert dict_eq({}, {})
    assert not dict_eq({}, {1: 3})
    assert dict_eq({"a": 2}, {"a": 2})
    assert not dict_eq({"a": {"a": 2}}, {"a": {"b": 2}})
    assert not dict_eq({"a": {"a": 2}}, {"a": {"b": 2, "a": 2}})
    assert dict_eq({"a": {"a": 3, "b": 2}}, {"a": {"b": 2, "a": 3}})
    assert not dict_eq({"a": []}, {"a": ()})
    assert dict_eq({"a": []}, {"a": []})
    assert not dict_eq({"a": [1]}, {"a": [2]})


def test_fake_routine_wrapper_construction():
    empty = FakeRoutineWrapper("empty")
    calls_empty = FakeRoutineWrapper("calls_empty", [empty])
    x = FakeRoutineWrapper("x")
    calls_empty_x = FakeRoutineWrapper("calls_empty_x", [x, calls_empty, empty, x])


def test_fake_routine_wrapper_empty():
    empty = FakeRoutineWrapper("empty")
    assert empty.name == "empty"
    assert empty.is_base
    assert empty.ops == []


def test_fake_routine_wrapper_x():
    x = FakeRoutineWrapper("x")
    calls_x = FakeRoutineWrapper("calls_x", [x])
    assert calls_x.name == "calls_x"
    assert not calls_x.is_base
    assert calls_x.ops == [x]
    assert list(calls_x) == [x]


def test_fake_routine_wrapper_multi_levels():
    x = FakeRoutineWrapper("x")
    calls_x = FakeRoutineWrapper("calls_x", [x])
    more_complex = FakeRoutineWrapper("more_complex", [x, calls_x, x, calls_x])
    assert more_complex.name == "more_complex"
    assert not more_complex.is_base
    assert len(more_complex.ops) == 4
    assert more_complex.ops[0].name == "x"
    assert more_complex.ops[0].is_base
    assert more_complex.ops[0] == more_complex.ops[2]
    assert more_complex.ops[1].name == "calls_x"
    assert not more_complex.ops[1].is_base
    assert more_complex.ops[1] == more_complex.ops[3]
    assert more_complex.ops[1].ops[0].name == "x"
    assert more_complex.ops[1].ops[0] == x
