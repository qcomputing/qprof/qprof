# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================
import typing as ty

from qprof.data import RoutineCallsData
from qprof.plugins.frameworks.interfaces import RoutineWrapper as BaseRoutineWrapper


def check_call_data(
    data: RoutineCallsData,
    number,
    self_nano_seconds: int,
    subroutines_nano_seconds: int,
) -> None:
    assert data.number == number
    assert data.self_nano_seconds == self_nano_seconds
    assert data.subroutines_nano_seconds == subroutines_nano_seconds


def _is_iterable(o) -> bool:
    try:
        iter(o)
    except TypeError:
        return False
    else:
        return True


def dict_eq(d1: dict, d2: dict) -> bool:
    if len(d1) != len(d2):
        return False
    if len(d1.keys() ^ d2.keys()) != 0:
        # Not the same keys
        return False
    for k in d1.keys():
        v1, v2 = d1[k], d2[k]
        if type(v1) is not type(v2):
            return False
        if isinstance(v1, dict):
            if not dict_eq(v1, v2):
                return False
        else:
            if v1 != v2:
                return False
    return True


class FakeRoutineWrapper(BaseRoutineWrapper):
    def __init__(
        self,
        name: str,
        calls: ty.List["FakeRoutineWrapper"] = None,
    ):
        if calls is None:
            calls = []

        super(FakeRoutineWrapper, self).__init__(name)
        self._name = name
        self._calls = calls

    def __iter__(self) -> ty.Iterable["FakeRoutineWrapper"]:
        yield from self._calls

    @property
    def is_base(self) -> bool:
        return len(self._calls) == 0

    @property
    def name(self) -> str:
        return self._name
