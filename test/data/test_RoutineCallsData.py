# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from qprof.data import RoutineCallsData
from test.utils import check_call_data, dict_eq


def test_construction_null():
    data = RoutineCallsData(0, 0, 0)
    check_call_data(data, 0, 0, 0)


def test_construction():
    data = RoutineCallsData(10, 56, 72)
    check_call_data(data, 10, 56, 72)


def test_addition_null_null():
    a, b = RoutineCallsData(0, 0, 0), RoutineCallsData(0, 0, 0)
    c = a + b
    check_call_data(a, 0, 0, 0)
    check_call_data(b, 0, 0, 0)
    check_call_data(c, 0, 0, 0)


def test_addition_null_non_null():
    a, b = RoutineCallsData(0, 0, 0), RoutineCallsData(10, 32, 90)
    c = a + b
    check_call_data(a, 0, 0, 0)
    check_call_data(b, 10, 32, 90)
    check_call_data(c, 10, 32, 90)


def test_addition_non_null_null():
    a, b = RoutineCallsData(4, 2, 8), RoutineCallsData(0, 0, 0)
    c = a + b
    check_call_data(a, 4, 2, 8)
    check_call_data(b, 0, 0, 0)
    check_call_data(c, 4, 2, 8)


def test_addition():
    a, b = RoutineCallsData(4, 2, 8), RoutineCallsData(78, 89, 43)
    c = a + b
    check_call_data(a, 4, 2, 8)
    check_call_data(b, 78, 89, 43)
    check_call_data(c, 82, 91, 51)


def test_iaddition_null_null():
    a, b = RoutineCallsData(0, 0, 0), RoutineCallsData(0, 0, 0)
    a += b
    check_call_data(a, 0, 0, 0)
    check_call_data(b, 0, 0, 0)


def test_iaddition_null_non_null():
    a, b = RoutineCallsData(0, 0, 0), RoutineCallsData(6, 86, 1)
    a += b
    check_call_data(a, 6, 86, 1)
    check_call_data(b, 6, 86, 1)


def test_iaddition_non_null_null():
    a, b = RoutineCallsData(56, 8, 4), RoutineCallsData(0, 0, 0)
    a += b
    check_call_data(a, 56, 8, 4)
    check_call_data(b, 0, 0, 0)


def test_iaddition():
    a, b = RoutineCallsData(76, 52, 8), RoutineCallsData(34, 2, 1)
    a += b
    check_call_data(a, 110, 54, 9)
    check_call_data(b, 34, 2, 1)


def test_to_dict_empty():
    a = RoutineCallsData(0, 0, 0)
    assert dict_eq(
        a.to_dict(),
        {"number": 0, "self_nano_seconds": 0, "subroutines_nano_seconds": 0},
    )


def test_to_dict():
    a = RoutineCallsData(1, 65, 8)
    assert dict_eq(
        a.to_dict(),
        {"number": 1, "self_nano_seconds": 65, "subroutines_nano_seconds": 8},
    )
