# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

from qprof.data import RoutineData, RoutineCallsData
from test.utils import check_call_data, dict_eq


def test_construction():
    data = RoutineData()
    check_call_data(data.self_call, 0, 0, 0)
    assert isinstance(data.called_by, dict)
    assert len(data.called_by) == 0
    assert isinstance(data.subroutine_calls, dict)
    assert len(data.subroutine_calls) == 0


def test_add_call_empty():
    data = RoutineData()
    data.add_call(RoutineCallsData(0, 0, 0))
    check_call_data(data.self_call, 0, 0, 0)


def test_add_call():
    data = RoutineData()
    data.add_call(RoutineCallsData(10, 45, 8))
    check_call_data(data.self_call, 10, 45, 8)
    data.add_call(RoutineCallsData(7, 89, 5))
    check_call_data(data.self_call, 17, 134, 13)


def test_add_subroutine_call_name_empty_empty():
    data = RoutineData()
    data.add_subroutine_call("", RoutineCallsData(0, 0, 0))


def test_add_subroutine_call_name_empty():
    data = RoutineData()
    data.add_subroutine_call("", RoutineCallsData(10, 67, 4))


def test_add_subroutine_call_name():
    data = RoutineData()
    data.add_subroutine_call("name", RoutineCallsData(0, 0, 0))
    assert "name" in data.subroutine_calls
    check_call_data(data.subroutine_calls["name"], 0, 0, 0)
    data.add_subroutine_call("name", RoutineCallsData(10, 67, 4))
    check_call_data(data.subroutine_calls["name"], 10, 67, 4)
    data.add_subroutine_call("name", RoutineCallsData(1, 4, 20))
    check_call_data(data.subroutine_calls["name"], 11, 71, 24)
    data.add_subroutine_call("different_name", RoutineCallsData(10, 89, 5))
    assert "different_name" in data.subroutine_calls
    check_call_data(data.subroutine_calls["different_name"], 10, 89, 5)
    check_call_data(data.subroutine_calls["name"], 11, 71, 24)


def test_add_called_by_name_empty_empty():
    data = RoutineData()
    data.add_called_by("", RoutineCallsData(0, 0, 0))


def test_add_called_by_name_empty():
    data = RoutineData()
    data.add_called_by("", RoutineCallsData(10, 67, 4))


def test_add_called_by_name():
    data = RoutineData()
    data.add_called_by("name", RoutineCallsData(0, 0, 0))
    assert "name" in data.called_by
    check_call_data(data.called_by["name"], 0, 0, 0)
    data.add_called_by("name", RoutineCallsData(10, 67, 4))
    check_call_data(data.called_by["name"], 10, 67, 4)
    data.add_called_by("name", RoutineCallsData(1, 4, 20))
    check_call_data(data.called_by["name"], 11, 71, 24)
    data.add_called_by("different_name", RoutineCallsData(10, 89, 5))
    assert "different_name" in data.called_by
    check_call_data(data.called_by["different_name"], 10, 89, 5)
    check_call_data(data.called_by["name"], 11, 71, 24)


def test_to_dict_empty():
    data = RoutineData()
    assert dict_eq(
        data.to_dict(),
        {
            "self_call": RoutineCallsData(0, 0, 0).to_dict(),
            "called_by": {},
            "subroutine_calls": {},
        },
    )


def test_to_dict_self_call():
    data = RoutineData()
    data.add_call(RoutineCallsData(5, 78, 42))
    assert dict_eq(
        data.to_dict(),
        {
            "self_call": RoutineCallsData(5, 78, 42).to_dict(),
            "called_by": {},
            "subroutine_calls": {},
        },
    )


def test_to_dict_called_by():
    data = RoutineData()
    data.add_called_by("caller", RoutineCallsData(5, 78, 42))
    assert dict_eq(
        data.to_dict(),
        {
            "self_call": RoutineCallsData(0, 0, 0).to_dict(),
            "called_by": {"caller": RoutineCallsData(5, 78, 42).to_dict()},
            "subroutine_calls": {},
        },
    )


def test_to_dict_subroutine_calls():
    data = RoutineData()
    data.add_subroutine_call("called", RoutineCallsData(5, 78, 42))
    assert dict_eq(
        data.to_dict(),
        {
            "self_call": RoutineCallsData(0, 0, 0).to_dict(),
            "called_by": {},
            "subroutine_calls": {"called": RoutineCallsData(5, 78, 42).to_dict()},
        },
    )
