# ======================================================================================
#
# Copyright: CERFACS, LIRMM, Total S.A. - the quantum computing team (March 2021)
# Contributor: Adrien Suau (adrien.suau@cerfacs.fr)
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your discretion) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License for more details. You should have received
# a copy of the GNU Lesser General Public License along with this program. If not, see
# https://www.gnu.org/licenses/lgpl-3.0.txt
#
# ======================================================================================

import pytest as pt

from qprof.data import ProgramData, RoutineData
from test.utils import check_call_data, dict_eq


def test_construction():
    _ = ProgramData(0)
    _ = ProgramData(10384739)
    _ = ProgramData(0, entry_point="")
    _ = ProgramData(0, entry_point="<main>")


def test_add_subroutine():
    data = ProgramData(0)
    data.add_subroutine("subroutine_name")
    assert "subroutine_name" in data.routines_data
    assert "subroutine_name" in data.indices
    check_call_data(data.routines_data["subroutine_name"].self_call, 0, 0, 0)
    assert len(data.routines_data["subroutine_name"].called_by) == 0
    assert len(data.routines_data["subroutine_name"].subroutine_calls) == 0


def test_add_subroutine_multiple():
    data = ProgramData(0)
    data.add_subroutine("subroutine_name")
    with pt.raises(RuntimeError):
        data.add_subroutine("subroutine_name")


def test_add_subroutine_index():
    data = ProgramData(0)
    for i in range(3):
        data.add_subroutine(str(i))
    for i in range(3):
        assert str(i) in data.indices
        assert data.indices[str(i)] == i


def test_add_subroutine_call_without_add_subroutine():
    data = ProgramData(0)
    with pt.raises(KeyError):
        data.add_subroutine_call("unknown", "unknown2", 0, 0, 0)
    data.add_subroutine("known")
    with pt.raises(KeyError):
        data.add_subroutine_call("known", "unknown", 0, 0, 0)
    with pt.raises(KeyError):
        data.add_subroutine_call("unknown", "known", 0, 0, 0)


def test_add_subroutine_call():
    data = ProgramData(0)
    data.add_subroutine("caller")
    data.add_subroutine("called")
    data.add_subroutine_call("caller", "called", 8, 63, 9)
    assert "caller" in data.routines_data
    assert "called" in data.routines_data
    assert "called" in data.routines_data["caller"].subroutine_calls
    assert "caller" in data.routines_data["called"].called_by
    check_call_data(data.routines_data["caller"].subroutine_calls["called"], 8, 63, 9)
    check_call_data(data.routines_data["called"].called_by["caller"], 8, 63, 9)
    check_call_data(data.routines_data["called"].self_call, 8, 63, 9)

    data.add_subroutine("called2")
    data.add_subroutine_call("caller", "called2", 67, 9, 34)
    assert "called2" in data.routines_data
    assert "called2" in data.routines_data["caller"].subroutine_calls
    assert "caller" in data.routines_data["called2"].called_by
    check_call_data(data.routines_data["caller"].subroutine_calls["called2"], 67, 9, 34)
    check_call_data(data.routines_data["called2"].called_by["caller"], 67, 9, 34)
    check_call_data(data.routines_data["called2"].self_call, 67, 9, 34)

    data.add_subroutine_call("called", "called2", 6, 533, 99)
    assert "called2" in data.routines_data["called"].subroutine_calls
    assert "called" in data.routines_data["called2"].called_by
    check_call_data(
        data.routines_data["called"].subroutine_calls["called2"], 6, 533, 99
    )
    check_call_data(data.routines_data["called2"].called_by["called"], 6, 533, 99)
    check_call_data(data.routines_data["called2"].self_call, 73, 542, 133)


def test_add_entry_point_call_without_add_subroutine():
    data = ProgramData(0)
    with pt.raises(KeyError):
        data.add_entry_point_call("unknown", 0, 0, 0)


def test_add_entry_point_call_multiple():
    data = ProgramData(0)
    data.add_subroutine("1")
    data.add_entry_point_call("1", 0, 0, 0)
    with pt.raises(RuntimeError):
        data.add_entry_point_call("1", 0, 0, 0)


def test_add_entry_point_call():
    data = ProgramData(0)
    data.add_subroutine("called")
    data.add_entry_point_call("called", 8, 63, 9)
    assert "called" in data.routines_data
    assert data.entry_point in data.routines_data["called"].called_by
    assert "called" in data.routines_data[data.entry_point].subroutine_calls
    check_call_data(
        data.routines_data[data.entry_point].subroutine_calls["called"], 8, 63, 9
    )
    check_call_data(data.routines_data["called"].called_by[data.entry_point], 8, 63, 9)
    check_call_data(data.routines_data["called"].self_call, 8, 63, 9)


def test_to_dict_empty():
    data = ProgramData(0)
    assert dict_eq(
        data.to_dict(),
        {
            "max_index": 0,
            "indices": {},
            "routines_data": {},
            "total_time_nanoseconds": 0,
            "entry_point": data.entry_point,
        },
    )


def test_to_dict_no_call():
    data = ProgramData(0)
    data.add_subroutine("routine")
    assert dict_eq(
        data.to_dict(),
        {
            "max_index": 0,
            "indices": {"routine": 0},
            "routines_data": {"routine": RoutineData().to_dict()},
            "total_time_nanoseconds": 0,
            "entry_point": data.entry_point,
        },
    )


def test_to_dict_call_entry_point():
    data = ProgramData(0)
    data.add_subroutine("routine")
    data.add_entry_point_call("routine", 10, 835, 8)
    assert data.to_dict() == {
        "max_index": 0,
        "indices": {"routine": 0},
        "routines_data": {k: v.to_dict() for k, v in data.routines_data.items()},
        "total_time_nanoseconds": 0,
        "entry_point": data.entry_point,
    }


def test_to_dict_call_subroutine():
    data = ProgramData(0)
    data.add_subroutine("caller")
    data.add_subroutine("called")
    data.add_subroutine_call("caller", "called", 10, 835, 8)
    assert data.to_dict() == {
        "max_index": 1,
        "indices": {"caller": 0, "called": 1},
        "routines_data": {k: v.to_dict() for k, v in data.routines_data.items()},
        "total_time_nanoseconds": 0,
        "entry_point": data.entry_point,
    }


def test_entry_point():
    data = ProgramData(0, "<newentrypoint>")
    assert data.entry_point == "<newentrypoint>"


def test_native_call_with_register():
    data = ProgramData(0, register_base_subroutine_calls=True)
    data.add_subroutine("u3")
    data.add_entry_point_call("u3", 1, 0, 80)
    with pt.raises(KeyError):
        data.add_base_subroutine_call("u3", "u", 1, 80, 0)
    data.add_subroutine("u")
    data.add_base_subroutine_call("u3", "u", 1, 80, 0)

    assert "u3" in data.indices
    assert "u" in data.indices
    assert "u3" in data.routines_data
    assert "u" in data.routines_data

    assert "u3" in data.routines_data[data.entry_point].subroutine_calls
    assert "u" not in data.routines_data[data.entry_point].subroutine_calls
    assert "u" in data.routines_data["u3"].subroutine_calls

    assert "u3" in data.routines_data["u"].called_by
    assert data.entry_point in data.routines_data["u3"].called_by

    check_call_data(data.routines_data["u"].self_call, 1, 80, 0)
    check_call_data(data.routines_data["u3"].subroutine_calls["u"], 1, 80, 0)
    check_call_data(data.routines_data["u"].called_by["u3"], 1, 80, 0)


def test_native_call_without_register():
    data = ProgramData(0, register_base_subroutine_calls=False)
    data.add_subroutine("u3")
    data.add_entry_point_call("u3", 1, 0, 80)
    data.add_base_subroutine_call("u3", "u", 1, 80, 0)

    assert "u3" in data.indices
    assert "u" not in data.indices
    assert "u3" in data.routines_data
    assert "u" not in data.routines_data

    assert "u3" in data.routines_data[data.entry_point].subroutine_calls
    assert "u" not in data.routines_data["u3"].subroutine_calls
    assert "u" not in data.routines_data

    assert data.entry_point in data.routines_data["u3"].called_by

    check_call_data(data.routines_data["u3"].self_call, 1, 80, 0)
