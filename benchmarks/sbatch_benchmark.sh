#!/bin/bash

# Number of cores to use. 
#SBATCH -c 1
# Standard output will be redirected to the file "job.[jobID]".
#SBATCH -o job.%j
# The job will run on the queue named "QLM" (the only queue available).
#SBATCH -p QLM

# Limit job runtime (helps scheduling).
#SBATCH --time=0-24:00:00


# Execute your executable (or Python program).

echo "================================================================================"
echo "Benchmark type : $1"
echo "Repetitions    : $2"
echo "Size:          : $3"
echo "================================================================================"

sbatch --job-name="qprof_benchmark.sh $1 $3 -r $2" benchmark.sh "$1" "$2" "$3"
