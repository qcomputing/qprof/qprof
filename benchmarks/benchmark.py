import typing as ty
from time import time

from qiskit.circuit.quantumcircuit import QuantumCircuit
from qprof import profile

from qat.lang.AQASM.program import Program
from qaths.applications.wave_equation.evolve_1D_dirichlet import evolve_1d_dirichlet
from qaths.applications.wave_equation.linking_sets.arithmetic_adder import (
    get_linking_set as arith_linking_set,
)

import numpy
from qiskit.algorithms import Shor, HHL
from qiskit.algorithms.linear_solvers.matrices import TridiagonalToeplitz

G = {"u1": 0, "u2": 89, "u3": 178, "cx": 930}
gate_times = {
    "u": G["u3"],
    "p": G["u1"],
    "cu1": 2 * G["cx"] + 2 * G["u1"],
    "cu2": 2 * G["cx"] + 2 * G["u3"],
    "X": G["u3"],
    "H": G["u2"],
    "CNOT": G["cx"],
    "CCNOT": 6 * G["cx"] + 2 * G["u2"] + 7 * G["u1"],
    "CH": 2 * G["cx"] + 2 * G["u3"],
    "PH": 3 * G["u1"] + 2 * G["cx"],
    "CPH": 3 * G["u1"] + 2 * G["cx"],
    "CCPH": None,
}
gate_times["CCPH"] = 3 * gate_times["CPH"] + 2 * gate_times["CCNOT"]
gate_times["CCCNOT"] = (
    6 * gate_times["CCNOT"] + 2 * gate_times["cu2"] + 7 * gate_times["cu1"]
)
gate_times.update(G)

format_string = "| {:>12} | {:>12} | {:>12} | {:>12} | {:>18} | {:>17} | {:>23} |"
format_string_numbers = "| {:>12} | {:>12} | {:>12} | {:>12} | {:>18.3f} | {:>6.2f} (±{:>6.2f})s | {:>9.2f} (±{:>9.2f})s |"
header = format_string.format(
    "algorithm",
    "qubit number",
    "repetitions",
    "gate number",
    "circuit generation",
    "profiling",
    "saved",
)
delimiter = "=" * len(header)


def print_header() -> None:
    print()
    print(delimiter)
    print(header)
    print(delimiter, flush=True)


def qiskit_get_gate_number_and_qubit_number(
    circuit: QuantumCircuit,
) -> ty.Tuple[ty.Dict[str, int], int]:
    previous_len = len(circuit)
    circuit = circuit.decompose()
    actual_len = len(circuit)
    consecutive_equal_lengths = 1 if actual_len == previous_len else 0
    while consecutive_equal_lengths < 3:
        circuit = circuit.decompose()
        actual_len, previous_len = len(circuit), actual_len
        if actual_len == previous_len:
            consecutive_equal_lengths += 1
        else:
            consecutive_equal_lengths = 0
    return circuit.count_ops(), circuit.num_qubits


def myqlm_get_gate_number_and_qubit_number(
    circuit, **kwargs
) -> ty.Tuple[ty.Dict[str, int], int]:

    program = Program()
    qbits = program.qalloc(circuit.arity)
    program.apply(circuit, qbits)
    circ = program.to_circ(inline=True, **kwargs)
    return circ.statistics()["gates"], circ.nbqbits


def wave_solver_get_gate_number_and_qubit_number(
    circuit, **kwargs
) -> ty.Tuple[ty.Dict[str, int], int]:
    trotter_steps = circuit.abstract_gate.circuit_generator(*circuit.parameters).op_list
    single_trotter_step = trotter_steps[0].gate

    gate_number = myqlm_get_gate_number_and_qubit_number(single_trotter_step, **kwargs)[
        0
    ]
    for k in gate_number:
        gate_number[k] *= len(trotter_steps)
    return gate_number, circuit.arity


def benchmark(
    algorithm_name: str,
    circuit_creation: ty.Callable,
    get_gate_count_and_qubit_number: ty.Callable,
    *args,
    repeat: int = 1,
    profile_kwargs: ty.Optional[ty.Dict[str, ty.Any]] = None,
    **kwargs,
):
    if profile_kwargs is None:
        profile_kwargs = {}
    args_str = ", ".join(map(str, args))
    kwargs_str = ", ".join(f"{k}={v}" for k, v in kwargs.items())
    arguments = f"{args_str}, {kwargs_str}".strip(", ")
    algorithm_name_and_parameters = algorithm_name + (
        f"({arguments})" if arguments else ""
    )
    data: ty.Dict[str, ty.Union[int, float, ty.List[ty.Union[float, int]]]] = {
        "depth": 0,
        "num_qubits": 0,
        "circuit_generation_time": 0,
        "qprof_time": [],
        "qprof_saved_time": [],
    }

    # Performing the computation once without saving anything.
    start_circuit = time()
    circuit = circuit_creation(*args, **kwargs)
    end_circuit = time()
    _, statistics = profile(circuit, gate_times, "gprof", **profile_kwargs)
    gate_count, qubit_number = get_gate_count_and_qubit_number(circuit)
    data["num_qubits"] = qubit_number
    data["gate_count"] = gate_count
    data["circuit_generation_time"] = end_circuit - start_circuit

    for _ in range(repeat):
        start_qprof = time()
        _, statistics = profile(circuit, gate_times, "gprof", **profile_kwargs)
        end_qprof = time()

        data["qprof_time"].append(end_qprof - start_qprof)
        data["qprof_saved_time"].append(statistics["saved_time"])

    print(
        format_string_numbers.format(
            algorithm_name_and_parameters
            if len(algorithm_name_and_parameters) <= 12
            else algorithm_name,
            data["num_qubits"],
            repeat,
            sum(data["gate_count"].values()),
            data["circuit_generation_time"],
            numpy.mean(data["qprof_time"]),
            numpy.std(data["qprof_time"]),
            numpy.mean(data["qprof_saved_time"]),
            numpy.std(data["qprof_saved_time"]),
        )
    )
    print(delimiter, flush=True)


def erathostene(N: int) -> numpy.ndarray:
    is_prime: numpy.ndarray = numpy.ones(N, dtype=bool)
    is_prime[0] = is_prime[1] = False
    is_prime[4::2] = False
    for i in range(3, int(numpy.ceil(numpy.sqrt(N))), 2):
        is_prime[2 * i :: i] = False
    return numpy.arange(N)[is_prime]


def benchmark_shor(N: int, repeat: int) -> None:
    shor = Shor()
    primes = erathostene(N)[1:]  # Removing 2 for Shor
    # Filter out values with the same number of bits
    shor_numbers_list = [int(p1 * p2) for p1, p2 in zip(primes[::2], primes[1::2])]
    shor_numbers_dict = dict()
    for n in shor_numbers_list:
        shor_numbers_dict[len(bin(n))] = n
    shor_numbers = list(shor_numbers_dict.values())

    for N in shor_numbers:
        benchmark(
            "Shor",
            shor.construct_circuit,
            qiskit_get_gate_number_and_qubit_number,
            N,
            repeat=repeat,
        )


def normalise(vector: numpy.ndarray) -> numpy.ndarray:
    return vector / numpy.linalg.norm(vector)


def benchmark_hhl(N: int, repeat: int) -> None:
    hhl = HHL()
    HHL_inputs = [
        (TridiagonalToeplitz(n, 1, 0.5), normalise(numpy.random.rand(2 ** n)))
        for n in range(1, N)
    ]
    for A, b in HHL_inputs:
        benchmark(
            "HHL",
            hhl.construct_circuit,
            qiskit_get_gate_number_and_qubit_number,
            A,
            b,
            repeat=repeat,
        )


def benchmark_wave(N: int, repeat: int) -> None:
    evolution_time = 0.1
    epsilon = 1e-3
    trotter_order = 1
    for exp in range(3, N):
        benchmark(
            "Wave",
            evolve_1d_dirichlet,
            lambda circ: wave_solver_get_gate_number_and_qubit_number(
                circ, link=arith_linking_set(2 ** exp)
            ),
            evolution_time,
            2 ** exp,
            epsilon,
            trotter_order,
            repeat=repeat,
            profile_kwargs={"linking_set": arith_linking_set(2 ** exp)},
        )


from argparse import ArgumentParser


def main() -> None:
    parser = ArgumentParser("Benchmark qprof")
    parser.add_argument(
        "benchmark",
        help="Benchmark to run",
        choices=["wave", "shor", "hhl"],
    )
    parser.add_argument(
        "N",
        type=int,
        help="Maximum size of the problem, see each benchmark for more information.",
    )
    parser.add_argument(
        "-r",
        "--repeat",
        type=int,
        default=10,
        help="Number of repetitions for each benchmark.",
    )

    args = parser.parse_args()

    glob = globals()
    if f"benchmark_{args.benchmark}" not in glob:
        raise RuntimeError(f"Cannot find benchmark_{args.benchmark}")
    benchmark_function = glob[f"benchmark_{args.benchmark}"]
    print_header()
    benchmark_function(args.N, args.repeat)


if __name__ == "__main__":
    main()


# In [3]:
# =======================================================================================================
# |    algorithm | qubit number |  repetitions | circuit generation |       profiling |           saved |
# =======================================================================================================
# |     Shor(15) |           18 |           10 |    1.651 (±0.892)s |  0.08 (± 0.00)s |  0.62 (± 0.01)s |
# =======================================================================================================
# |     Shor(77) |           30 |           10 |    7.346 (±1.002)s |  0.22 (± 0.01)s |  2.89 (± 0.05)s |
# =======================================================================================================
# |    Shor(221) |           34 |           10 |   10.744 (±0.160)s |  0.26 (± 0.01)s |  3.85 (± 0.15)s |
# =======================================================================================================
# |    Shor(437) |           38 |           10 |   15.463 (±1.299)s |  0.53 (± 0.72)s |  8.96 (±12.11)s |
# =======================================================================================================
# |    Shor(899) |           42 |           10 |   21.117 (±1.234)s |  0.59 (± 0.76)s | 10.94 (±14.05)s |
# =======================================================================================================
# |   Shor(1517) |           46 |           10 |   28.459 (±2.057)s |  0.40 (± 0.01)s |  7.98 (± 0.16)s |
# =======================================================================================================
# |   Shor(2021) |           46 |           10 |   28.418 (±1.192)s |  0.40 (± 0.01)s |  8.00 (± 0.07)s |
# =======================================================================================================
# |   Shor(3127) |           50 |           10 |   37.150 (±1.647)s |  0.44 (± 0.01)s |  9.41 (± 0.06)s |
# =======================================================================================================
# |   Shor(4087) |           50 |           10 |   37.213 (±1.633)s |  0.45 (± 0.01)s |  9.71 (± 0.14)s |
# =======================================================================================================
# |          HHL |            4 |           10 |    0.275 (±0.654)s |  0.01 (± 0.00)s |  0.08 (± 0.01)s |
# =======================================================================================================
# |          HHL |            8 |           10 |    0.555 (±0.552)s |  0.03 (± 0.00)s |  0.74 (± 0.04)s |
# =======================================================================================================
# |          HHL |           12 |           10 |    1.961 (±0.929)s |  0.06 (± 0.00)s |  1.43 (± 0.06)s |
# =======================================================================================================
# |          HHL |           15 |           10 |   10.927 (±0.985)s |  0.12 (± 0.00)s |  6.14 (± 0.33)s |
# =======================================================================================================
# |          HHL |           19 |           10 |   31.529 (±1.812)s |  0.64 (± 0.97)s | 12.73 (± 0.96)s |
# =======================================================================================================
# |          HHL |           23 |           10 |   89.312 (±2.336)s |  0.93 (± 0.02)s | 27.60 (± 1.51)s |
# =======================================================================================================


# ===============================================================================================================================
# |    algorithm | qubit number |  repetitions |  gate number | circuit generation |         profiling |                  saved |
# ===============================================================================================================================
# |         Wave |            4 |           10 |       126846 |    0.000 (±0.000)s |   0.01 (±  0.00)s |       0.56 (±   0.01)s |
# ===============================================================================================================================
# |         Wave |            5 |           10 |       528768 |    0.000 (±0.000)s |   0.01 (±  0.00)s |       2.03 (±   0.03)s |
# ===============================================================================================================================
# |         Wave |            6 |           10 |      1953720 |    0.000 (±0.000)s |   0.02 (±  0.00)s |       6.88 (±   0.07)s |
# ===============================================================================================================================
# |         Wave |            7 |           10 |      6773868 |    0.000 (±0.000)s |   0.05 (±  0.00)s |      20.44 (±   1.75)s |
# ===============================================================================================================================
# |         Wave |            8 |           10 |     22575672 |    0.000 (±0.000)s |   0.13 (±  0.05)s |      64.04 (±   5.69)s |
# ===============================================================================================================================
# |         Wave |            9 |           10 |     73323792 |    0.000 (±0.000)s |   0.36 (±  0.08)s |     199.98 (±  19.49)s |
# ===============================================================================================================================
# |         Wave |           10 |           10 |    233816544 |    0.000 (±0.000)s |   0.99 (±  0.09)s |     659.12 (±  47.60)s |
# ===============================================================================================================================
# |         Wave |           11 |           10 |    735473520 |    0.000 (±0.000)s |   2.88 (±  0.10)s |    2128.86 (±  24.24)s |
# ===============================================================================================================================
# |         Wave |           12 |           10 |   2289028896 |    0.000 (±0.000)s |   8.30 (±  0.14)s |    6469.88 (±  39.74)s |
# ===============================================================================================================================
# |         Wave |           13 |           10 |   7063525944 |    0.000 (±0.000)s |  28.16 (±  1.92)s |   21293.68 (±2461.04)s |
# ===============================================================================================================================
# |         Wave |           14 |           10 |  21643231428 |    0.000 (±0.000)s |  77.20 (±  2.04)s |   59477.56 (± 931.80)s |
# ===============================================================================================================================
# |         Wave |           15 |           10 |  65922050880 |    0.000 (±0.000)s | 233.00 (± 28.85)s | 190452.51 (±18564.09)s |
# ===============================================================================================================================
