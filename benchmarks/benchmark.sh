#!/bin/bash

# Number of cores to use. 
#SBATCH -c 1
# Standard output will be redirected to the file "job.[jobID]".
#SBATCH -o job.%j
# The job will run on the queue named "QLM" (the only queue available).
#SBATCH -p QLM

# Limit job runtime (helps scheduling).
#SBATCH --time=0-24:00:00


# Execute your executable (or Python program).

echo "================================================================================"
echo "Benchmark type : $1"
echo "Repetitions    : $2"
echo "Size:          : $3"
echo "================================================================================"

source /data_lab/users/a.suau/.bashrc
source /data_lab/users/a.suau/qprof/venv/qprof/bin/activate

python /data_lab/users/a.suau/qprof/qprof/benchmarks/benchmark.py "$1" "$3" -r "$2"
